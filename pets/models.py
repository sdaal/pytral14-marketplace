from django.db import models


class Pet(models.Model):
    name = models.CharField(max_length=64)
    age = models.IntegerField(default=0)
    photo = models.ImageField(upload_to='pets')
    breed = models.CharField(max_length=128)

    class Meta:
        db_table = 'pets'

    def __str__(self):
        return self.name
