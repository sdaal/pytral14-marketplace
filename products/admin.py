from django.contrib import admin

from .models import Product, Tag, Picture


class TagInline(admin.TabularInline):
    model = Tag
    max_num = 12


class PicturesInline(admin.TabularInline):
    model = Picture
    max_num = 20


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ["title", "is_published", "views", "telno"]
    prepopulated_fields = {
        "slug": ("title",)
    }
    inlines = [TagInline, PicturesInline]
