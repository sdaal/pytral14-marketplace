from django.views import generic
from django.urls import reverse_lazy

from .models import Pet


class PetListView(generic.ListView):
    model = Pet


class PetCreateView(generic.CreateView):
    model = Pet
    fields = ['name', 'age', 'photo', 'breed']
    success_url = reverse_lazy('pet-list')


class PetDetailView(generic.DetailView):
    model = Pet


class PetUpdateView(generic.UpdateView):
    model = Pet
    fields = ['name', 'age', 'photo', 'breed']
    success_url = reverse_lazy('pet-list')


class PetDeleteView(generic.DeleteView):
    model = Pet
    success_url = reverse_lazy('pet-list')
