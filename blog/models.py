from django.db import models
from django.urls import reverse
from django.utils.text import slugify
from django.conf import settings


class Category(models.Model):   # blog_category
    name = models.CharField(max_length=32, null=False, blank=False, unique=True)
    slug = models.SlugField(max_length=32)

    class Meta:
        db_table = "categories"
        verbose_name = "category"
        verbose_name_plural = "categories"

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super().save(*args, **kwargs)


class Post(models.Model):   # default table name: blog_post
    title = models.CharField(max_length=256, null=False, blank=False)
    slug = models.SlugField(max_length=256)
    body = models.TextField(null=False, blank=False)
    is_published = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    published_at = models.DateTimeField(null=True, blank=True)
    category = models.ForeignKey(Category, null=True, related_name='posts', on_delete=models.SET_NULL)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, related_name="posts", on_delete=models.SET_NULL)

    class Meta:
        db_table = 'posts'

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('post-detail', kwargs={"slug": self.slug})


class Comment(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='comments',
        on_delete=models.CASCADE,
    )
    post = models.ForeignKey(
        Post,
        related_name='comments',
        on_delete=models.CASCADE,
    )
    comment = models.TextField()
    is_approved = models.BooleanField(default=False)
    commented_on = models.DateTimeField(auto_now_add=True)
    approved_on = models.DateTimeField(null=True)

    class Meta:
        db_table = 'comments'
        permissions = [
            ("can_approve_comment", "Can approve comment"),
        ]
