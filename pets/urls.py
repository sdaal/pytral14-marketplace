from django.urls import path

from . import views

urlpatterns = [
    path('', views.PetListView.as_view(), name='pet-list'),
    path('add/', views.PetCreateView.as_view(), name='pet-create'),
    path('<int:pk>/', views.PetDetailView.as_view(), name="pet-detail"),
    path('<int:pk>/edit/', views.PetUpdateView.as_view(), name="pet-update"),
    path('<int:pk>/delete/', views.PetDeleteView.as_view(), name="pet-delete"),
]
