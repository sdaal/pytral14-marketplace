from django.shortcuts import render
from django.http import HttpRequest, HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.utils import timezone

from .models import Product
from .forms import ProductForm, TagFormSet, PictureFormSet


@login_required
def add_product(request: HttpRequest) -> HttpResponse:
    print("FILES::KEYS:", request.FILES.keys())
    if request.method == 'POST':
        form = ProductForm(request.POST)
        tag_formset = TagFormSet(request.POST)
        pics_formset = PictureFormSet(request.POST, request.FILES)
        if form.is_valid() \
                and tag_formset.is_valid() \
                and pics_formset.is_valid():
            product = form.save(commit=False)
            if product.is_published:
                product.published_at = timezone.now()
            product.user = request.user
            product.save()

            tag_formset.instance = product
            tag_formset.save()

            pics_formset.instance = product
            pics_formset.save()

            return HttpResponseRedirect("/")
    else:
        form = ProductForm()
        tag_formset = TagFormSet()
        pics_formset = PictureFormSet()
    return render(request, "products/product_form.html", context={
        "form": form,
        "tag_formset": tag_formset,
        "pics_formset": pics_formset,
    })


def get_product_list(request: HttpRequest) -> HttpResponse:
    products = (
        Product.objects
        .filter(is_published=True)
        .filter(is_active=True)
        .order_by('-published_at')
        .all()
        )
    return render(request, "products/product_list.html", context={
        "products": products
    })


def get_product_detail(request, slug):
    product = (
        Product.objects
        .filter(is_active=True)
        .filter(is_published=True)
        .get(slug=slug)
    )
    return render(request, 'products/product_detail.html', context={
        "product": product,
    })
