from django.contrib import admin

from .models import Post, Category


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {
        "slug": ["name"]
    }


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ["title", "is_published", "category"]
    prepopulated_fields = {
        "slug": ("title",)
    }
    list_editable = ["is_published"]

    def get_queryset(self, request):
        return super().get_queryset(request).select_related('category')
