from django import forms

from .models import Category, Comment


class PostForm(forms.Form):
    title = forms.CharField(min_length=5, max_length=256)
    body = forms.CharField(widget=forms.Textarea, min_length=42)
    category = forms.ModelChoiceField(queryset=Category.objects.all())


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['comment']