from django.urls import path

from . import views

urlpatterns = [
    path("", views.get_product_list, name="product-list"),
    path('products/add', views.add_product, name="product-add"),
    path('products/<slug>/', views.get_product_detail, name='product-detail'),
]
