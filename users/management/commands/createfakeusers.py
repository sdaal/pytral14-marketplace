from django.core.management.base import BaseCommand
from argparse import ArgumentParser
from faker import Faker
from django.contrib.auth import get_user_model

User = get_user_model()


class Command(BaseCommand):
    def add_arguments(self, parser: ArgumentParser):
        parser.add_argument("--count", '-c', type=int, default=10)

    def handle(self, *args, **options):
        fake = Faker()
        for i in range(options['count']):
            user = User.objects.create_user(
                username=fake.user_name(),
                email=fake.email(),
                password='pass123',
                first_name=fake.first_name(),
                last_name=fake.last_name(),
            )
            self.stdout.write(
                self.style.HTTP_INFO(f'Created user: {user.username}')
            )
