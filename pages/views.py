from django.shortcuts import render
from django.http import HttpRequest, HttpResponse

from .forms import VolunteerForm


def home(request: HttpRequest) -> HttpResponse:
    return render(request, 'pages/home.html')


def about_us(request: HttpRequest) -> HttpResponse:
    request.session["views"] = request.session.get("views", {})
    request.session["views"]["about"] = request.session["views"].get('about', 0) + 1
    request.session.modified = True
    return render(request, 'pages/about_us.html')


def contact_us(request: HttpRequest) -> HttpResponse:
    request.session["views"] = request.session.get("views", {})
    request.session["views"]["contact"] = request.session["views"].get('contact', 0) + 1
    return render(request, 'pages/contact_us.html')


def privacy_policy(request: HttpRequest) -> HttpResponse:
    request.session["views"] = request.session.get("views", {})
    request.session["views"]["privacy"] = request.session["views"].get('privacy', 0) + 1
    return render(request, 'pages/privacy_policy.html')


def volunteer(request):
    if request.method == 'POST':
        form = VolunteerForm(request.POST)
        if form.is_valid():
            pass
    else:
        form = VolunteerForm()
    return render(request, "pages/volunteer.html", context={
        'form': form,
    })
