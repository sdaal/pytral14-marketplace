from django.shortcuts import render, redirect
from django.http import HttpRequest, HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required, permission_required

from .models import Comment, Post
from .forms import CommentForm, PostForm


def get_post_list(request: HttpRequest) -> HttpResponse:
    posts = (
        Post.objects
        .select_related('category')
        .filter(is_published=True)
        .all()
    )
    return render(request, 'blog/post_list.html', context={
        "posts": posts,
    })


def get_post(request, slug):
    post = Post.objects.get(slug=slug)
    comments = post.comments.filter(is_approved=True).all()
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.user = request.user
            comment.post = post
            comment.save()
            return redirect(post)
    else:
        form = CommentForm()
    return render(request, 'blog/post_detail.html', context={
        "post": post,
        "comments": comments,
        "form": form,
    })


@login_required
def add_post(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post = Post(
                title=form.cleaned_data["title"],
                body=form.cleaned_data["body"],
                category=form.cleaned_data["category"],
                author=request.user,
            )
            post.save()
            # return redirect('post-detail', slug=post.slug)
            return redirect(post)
    else:
        form = PostForm()
    return render(request, 'blog/post_form.html', context={
        "form": form,
    })


def edit_post(request, slug):
    post = Post.objects.get(slug=slug)
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post.title = form.cleaned_data["title"]
            post.body = form.cleaned_data["body"]
            post.category = form.cleaned_data["category"]
            post.save()
            # return redirect('post-detail', slug=post.slug)
            return redirect(post)
    else:
        form = PostForm(initial={
            "title": post.title,
            "body": post.body,
            "category": post.category,
        })
    return render(request, 'blog/post_form.html', context={
        "form": form,
        "post": post,
    })


def delete_post(request, slug):
    post = Post.objects.get(slug=slug)
    if request.method == 'POST':
        post.delete()
        return redirect('post-list')
    return render(request, "blog/post_confirm_delete.html", context={
        "post": post,
    })


@permission_required('blog.can_approve_comment')
def show_comments_for_approval(request):
    comments = Comment.objects.filter(is_approved=False).all()
    return render(request, "blog/comments.html", context={
        "comments": comments,
    })
