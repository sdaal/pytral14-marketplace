from django.urls import path

from . import views

urlpatterns = [
    path('posts/', views.get_post_list, name='post-list'),
    path('posts/new/', views.add_post, name="post-add"),
    path('comments-waiting-for-approval/', views.show_comments_for_approval, name="show-comments"),
    path('posts/<slug>/', views.get_post, name="post-detail"),
    path('posts/<slug>/delete/', views.delete_post, name="post-delete"),
    path('posts/<slug>/edit/', views.edit_post, name="post-edit"),
]
