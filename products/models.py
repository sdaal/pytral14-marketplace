from django.db import models
from django.conf import settings
from django.utils.translation import gettext_lazy as _
from django.utils.text import slugify


class Product(models.Model):
    class Currency(models.TextChoices):
        ALL = 'ALL', _('LEK')
        EUR = 'EUR', _('EUR')
        USD = 'USD', _('USD')

    title = models.CharField(_('title'), max_length=256)
    slug = models.SlugField(_("slug"), max_length=256, unique=True)
    description = models.TextField(_('description'))
    is_published = models.BooleanField(_('is published?'), default=False)
    published_at = models.DateTimeField(_('published at'), null=True, blank=True)
    views = models.IntegerField(_("views"), default=0)
    created_at = models.DateTimeField(_("created at"), auto_now_add=True)
    updated_at = models.DateTimeField(_("updated at"), auto_now=True)
    is_active = models.BooleanField(_("is active?"), default=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_("user"),
                             on_delete=models.CASCADE, related_name="products")
    telno = models.CharField(_("telephone number"), max_length=32)
    hide_telno = models.BooleanField(default=False)
    price = models.DecimalField(_("price"), max_digits=12, decimal_places=2)
    currency = models.CharField(_("currency"), max_length=3,
                                choices=Currency.choices,
                                default=Currency.ALL)

    class Meta:
        db_table = "products"
        verbose_name = _("product")
        verbose_name_plural = _("products")

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super().save(*args, **kwargs)


class Tag(models.Model):
    key = models.CharField(_("key"), max_length=32)
    value = models.CharField(_("value"), max_length=32)
    product = models.ForeignKey(Product, verbose_name=_("product"),
                                on_delete=models.CASCADE, related_name="tags")

    class Meta:
        db_table = "tags"
        verbose_name = _("tag")
        verbose_name_plural = _("tags")

    def __str__(self):
        return f'{self.key}: {self.value}'


class Picture(models.Model):
    product = models.ForeignKey(Product, verbose_name=_("product"),
                                on_delete=models.CASCADE, related_name="pictures")
    photo = models.ImageField(_("photo"), null=False, blank=False,
                              upload_to='pictures/%Y/%m/%d')
    caption = models.CharField(_("caption"), max_length=140, blank=True)

    class Meta:
        db_table = 'pictures'
        verbose_name = _("picture")
        verbose_name_plural = _("pictures")

    def __str__(self):
        return self.caption
