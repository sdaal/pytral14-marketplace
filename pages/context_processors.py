import random

from django.conf import settings


def show_ad(request):
    ad = random.choice(settings.ADS)
    return {
        "current_ad": ad
    }
