from django import forms

from .models import Product, Tag, Picture


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = (
            'title',
            'description',
            'is_published',
            'telno',
            'hide_telno',
            'price',
            'currency',
        )


TagFormSet = forms.inlineformset_factory(
    Product,
    Tag,
    fields=['key', 'value'],
    max_num=12,
)

PictureFormSet = forms.inlineformset_factory(
    Product,
    Picture,
    fields=['photo', 'caption'],
    max_num=20,
)
