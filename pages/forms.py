import datetime

from django import forms
from django.utils.translation import gettext as _, gettext_lazy as __


def validate_name(value):
    if value.startswith('W') or value.startswith('w'):
        raise forms.ValidationError(_('Go away you weirdo!'))


class VolunteerForm(forms.Form):
    name = forms.CharField(
        min_length=5,
        max_length=100,
        required=True,
        validators=[validate_name],
    )
    birthdate = forms.DateField(required=True)
    bmi = forms.IntegerField(required=True, min_value=20, max_value=27, error_messages={
        'min_value': __('At least 20 BMI'),
        'max_value': __('At most 27 BMI'),
    })
    gender = forms.ChoiceField(choices=(
        ("M", __("Male")),
        ("F", __("Female")),
    ), required=True)
    has_pilot_license = forms.BooleanField(required=False)

    def clean_birthdate(self):
        birthdate = self.cleaned_data['birthdate']
        today = datetime.date.today()
        delta = today - birthdate
        age = int(delta.days / 365.25)
        if not (18 <= age <= 45):
            raise forms.ValidationError(_("Only people from 18 to 45 can enlist"))
        return birthdate

    # def clean_bmi(self):
    #     bmi = self.cleaned_data['bmi']
    #     if bmi < 20:
    #         raise forms.ValidationError(_("You are underweight. Eat something and try again!"))
    #     elif bmi > 27:
    #         raise forms.ValidationError(_("You are overweight/obese."))
    #     return bmi

    def clean_gender(self):
        gender = self.cleaned_data["gender"]
        if gender != "M":
            raise forms.ValidationError(_("Only males accepted at this time"))
        return gender

    def clean(self):
        super().clean()
        today = datetime.date.today()
        age = int((today - self.cleaned_data.get('birthdate', today).days / 365.25))
        bmi = self.cleaned_data.get('bmi', 100)
        has_license = self.cleaned_data.get('has_pilot_license', False)
        if age == 45 and bmi == 27 and not has_license:
            raise forms.ValidationError(_("You need a pilot license when you are 45 and almost overweight"))
        return self.cleaned_data
